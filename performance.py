import matplotlib.pyplot as plt
import os
import numpy as np


""" substitute some strings to adjust clever&clover output to be similar """
SUBSTITUTIONS = {
    " : ": "",
    "Total": "wallclock",                                           # -> clover time
    "TOTAL RUN TIME:": "wallclock",                                 # -> clever time
    "MPI Halo Exchange": "communication",                           # -> clover comm
    "xfer::RefineSchedule::fillData()_recursive": "communication"   # -> clever comm
}

""" these are the categories I from the output files that I care about """
INTERESTING_CATEGORIES = [
    "wallclock",                                                    # -> wallclock time
    "communication"                                                 # -> communication time
    ]

MAXIMUM = "max"                                                     # -> max between all mpi ranks
AVERAGE = "avg"                                                     # -> avg between all mpi ranks
SUM = "sum"                                                         # -> sum between all mpi ranks
PERCENTAGE = "per"                                                  # -> sum / mpi rank num

TASK = 0                                                            # -> xlabel plots by task number
THREAD = 1                                                          # -> xlabel plots by thread number
COMBINED = 2                                                        # -> xlabel plots by threads X tasks


class TimerAnalyzer:

    """
    We assume here the following hierarchy of files:

    ------------------ CLEVERLEAF ------------------
        <timer_stats_directory>
            1_threads_mpi_rank_1
                - statistics_0
            ...
            <threads>_threads_mpi_rank_<rank>
                - statistics_0
                - ...
                - statistics_<rank>    
    ------------------ CLOVERLEAF ------------------
        <timer_stats_directory>
            1_threads_mpi_rank_1
            ...
            <threads>_threads_mpi_rank_<rank>
    """
    def __init__(self, timer_stats_directory, leaf):
        self.leaf = leaf
        self.timer_stats_directory = timer_stats_directory
        self.ranks = self.extract_ranks(timer_stats_directory) # -> list of tuples (<tasks>, <threads>)
        self.ranks_data = {}
        for rank in self.ranks:
            if leaf == "CLEVER":
                self.ranks_data[rank] = self.extract_data_clever(rank)
            elif leaf == "CLOVER":
                self.ranks_data[rank] = self.extract_data_clover(rank)


    def lines_substitude(self, data_file):
        data = data_file.read()
        if "Profiler Output" in data:
            data = data[data.index("Profiler Output"):]
        for key in SUBSTITUTIONS:
            data = data.replace(key, SUBSTITUTIONS[key])
        return data.splitlines()

    
    def extract_ranks(self, timer_stats_directory):
        return sorted([(int(dir.split("_")[-1]), int(dir.split("_")[0]), int(dir.split("_")[-1]) * int(dir.split("_")[0])) for dir in os.listdir(timer_stats_directory) if not dir.endswith(".png")])
    
    def extract_data_clever(self, rank):
        data_dict = {}
        for i in range(rank[TASK]):
            data_dict[i] = {}
            with open(self.timer_stats_directory + "/" + str(rank[THREAD]) + "_threads_mpi_rank_" + str(rank[TASK]) + "/statistics_" + str(i), "r") as data_file:
                lines = self.lines_substitude(data_file)
                for line in lines:
                    for category in INTERESTING_CATEGORIES:
                        if category in line:
                            data_dict[i][category] = " ".join(line[len(category):].split()).split(" ")[0]
        return data_dict
    
    def extract_data_clover(self, rank):
        data_dict = {}
        for i in range(rank[TASK]):
            data_dict[i] = {}
            if i != rank[TASK] - 1:
                for category in INTERESTING_CATEGORIES:
                    data_dict[i][category] = 0
            else:
                with open(self.timer_stats_directory + "/" + str(rank[THREAD]) + "_threads_mpi_rank_" + str(i + 1), "r") as data_file:
                    lines = self.lines_substitude(data_file)
                    for line in lines:
                        for category in INTERESTING_CATEGORIES:
                            if category in line:
                                data_dict[i][category] = " ".join(line[len(category):].split()).split(" ")[0]
        self.copy_values(data_dict, rank)        
        return data_dict
    

    def copy_values(self, data_dict, rank):
        for i in range(rank[TASK] - 1):
            for category in INTERESTING_CATEGORIES:
                data_dict[i][category] = data_dict[rank[TASK] - 1][category]
    

    def get_ranks(self):
        return self.ranks

    def get_total_category_time_core_num(self, core_num, category):
        return sum([float(self.ranks_data[core_num][rank][category]) for rank in self.ranks_data[core_num]])
    
    def get_max_category_time_core_num(self, core_num, category):
        return max([float(self.ranks_data[core_num][rank][category]) for rank in self.ranks_data[core_num]])

    def get_total_runtime_core_num(self, core_num):
        return self.get_total_category_time_core_num(core_num, "wallclock")
    
    def get_total_comm_time_core_num(self, core_num):
        return self.get_total_category_time_core_num(core_num, "communication")
    

    def get_avg_category_time_core_num(self, core_num, category):
        return self.get_total_category_time_core_num(core_num, category) / core_num[TASK]

    def get_avg_category_perc_cor_num(self, core_num, category):
        return (self.get_total_category_time_core_num(core_num, category) / self.get_total_runtime_core_num(core_num)) * 100

    def get_avg_comm_core_num(self, core_num):
        return self.get_avg_category_time_core_num(core_num, "communication")

    def get_avg_runtime_core_num(self, core_num):
        return self.get_avg_category_time_core_num(core_num, "wallclock")
    
    
    def get_all_category_data(self, category, mode=AVERAGE):
        if mode == AVERAGE:
            return [self.get_avg_category_time_core_num(rank, category) for rank in self.ranks]
        elif mode == MAXIMUM:
            return [self.get_max_category_time_core_num(rank, category) for rank in self.ranks]
        elif mode == SUM:
            return [self.get_total_category_time_core_num(rank, category) for rank in self.ranks]
        elif mode == PERCENTAGE:
            return [self.get_avg_category_perc_cor_num(rank, category) for rank in self.ranks]
        else:
            print(mode, "is not supported")
            exit(1)

    
    def get_parallel_efficiency(self, by=TASK):
        x = [rank[by] for rank in self.ranks]
        wallclock = self.get_all_category_data("wallclock")
        efficiency = []
        for i in range(len(x)):
            efficiency.append(100 * wallclock[0] / (wallclock[i] * x[i]))
        return efficiency
    
    def get_speedup_ratio(self, by=TASK):
        x = [rank[by] for rank in self.ranks]
        wallclock = self.get_all_category_data("wallclock")
        ratio = []
        for i in range(len(x)):
            ratio.append(wallclock[0] / wallclock[i])
        return ratio

    
    def plot_all_by_category(self, category, fig_name, title, ylim_factor=1.5, mode=AVERAGE, by=TASK, category_data=None):
        if category_data == None:
            category_data = self.get_all_category_data(category, mode)
        x = [rank[by] for rank in self.ranks]
        xlabel = "cores" if by == TASK else "threads"
        plt.plot(x, category_data)
        plt.scatter(x, category_data)
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(category + ";" + mode)
        #plt.ylim(0, max(category_data) * ylim_factor)
        plt.savefig(self.timer_stats_directory + "/" + fig_name + ".png")
        plt.clf()



def plot_data(x_list, y_list, xlabel, ylabel, labels, fig_full_path, title, ylim_factor=1.5, bar=False):
    for i in range(len(x_list)):
        if bar:
            plt.bar(x_list[i], y_list[i], align='center')
            plt.xticks(x_list[i], labels, rotation=-15)
        else:
            plt.plot(x_list[i], y_list[i], label=labels[i])
            plt.scatter(x_list[i], y_list[i])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    #plt.ylim(0, np.max(y_list) * ylim_factor)
    plt.legend()
    plt.savefig(fig_full_path + ".png")
    plt.clf()


def plot_combind_data(timer_list, category, xlabel, ylabel, labels, fig_full_path, fig_title, by=TASK):
    index = 0
    for timer in timer_list:
        xs = [x[by] for x in timer.get_ranks()]
        ys = timer.get_all_category_data(category)
        plt.plot(xs, ys, label=labels[index])
        plt.scatter(xs, ys)
        index += 1
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(fig_title)
    plt.legend()
    plt.savefig(fig_full_path + ".png")
    plt.clf()


def filter(x, y, lim):
    to_delete = []
    for i in range(len(x)):
        if x[i] > lim:
            to_delete.append((x[i], y[i]))
    for element in to_delete:
        x.remove(element[0])
        y.remove(element[1])


"""
t1 = TimerAnalyzer("/home/xabenshahar/data/gnu/CleverLeaf_opt/weak_1500x1500_15", "CLEVER")
t2 = TimerAnalyzer("/home/xabenshahar/data/intel/CleverLeaf/weak_1500x1500_15", "CLEVER")
labels = ["1500x1500_gnu", "1500x1500_intel"]
fig_full_path = "/home/xabenshahar/data/gnu/weak_1500_15"
plot_combind_data([t1, t2], "wallclock", "mpi ranks", "wallclock time", labels, fig_full_path, "cleverleaf weak scale")
"""


"""
t1 = TimerAnalyzer("/home/xabenshahar/data/gnu/CleverLeaf_opt/strong_3500x3500_platinum", "CLEVER")
t2 = TimerAnalyzer("/home/xabenshahar/data/gnu/CleverLeaf_opt/strong_3500x3500_gold", "CLEVER")
labels = ["3500x3500_platinum", "3500x3500_gold"]
fig_full_path = "/home/xabenshahar/data/gnu/strong_3500"
plot_combind_data([t1, t2], "wallclock", "mpi ranks", "wallclock time", labels, fig_full_path, "cleverleaf strong scale gnu")


t1 = TimerAnalyzer("/home/xabenshahar/data/intel/CleverLeaf/strong_3500x3500_platinum", "CLEVER")
t2 = TimerAnalyzer("/home/xabenshahar/data/intel/CleverLeaf/strong_3500x3500_gold", "CLEVER")
labels = ["3500x3500_platinum", "3500x3500_gold"]
fig_full_path = "/home/xabenshahar/data/intel/strong_3500"
plot_combind_data([t1, t2], "wallclock", "mpi ranks", "wallclock time", labels, fig_full_path, "cleverleaf strong scale intel")
"""


"""
t1 = TimerAnalyzer("/home/xabenshahar/data/gnu/CleverLeaf_opt/weak_2000x2000_platinum", "CLEVER")
t2 = TimerAnalyzer("/home/xabenshahar/data/intel/CleverLeaf/weak_2000x2000_platinum", "CLEVER")

labels = ["2000x2000_gnu", "2000x2000_intel"]
fig_full_path = "/home/xabenshahar/data/weak_2000"
plot_combind_data([t1, t2], "wallclock", "mpi ranks", "wallclock time", labels, fig_full_path, "cleverleaf weak scale")
"""


"""
t1 = TimerAnalyzer("/home/xabenshahar/data/gnu/CleverLeaf_opt/hybrid_50000x50000_platinum", "CLEVER")

xs = [x[COMBINED] for x in t1.get_ranks()]
ys = t1.get_all_category_data("wallclock")

#labels = ["1 omp X 360 mpi", "2 omp X 180 mpi", "4 omp X 90 mpi", "9 omp X 40 mpi", "18 omp X 20 mpi" , "36 omp X 10 mpi"]
labels = ["1 omp X 1080 mpi", "2 omp X 540 mpi", "4 omp X 270 mpi", "9 omp X 120 mpi", "18 omp X 60 mpi" , "36 omp X 30 mpi"]
labels.reverse()
plot_data([labels], [ys], "mpi X omp", "runtime (seconds)", labels, "/home/xabenshahar/data/gnu/CleverLeaf_opt/hybrid_50000x50000_platinum/wallclock", "cleverleaf hybrid", bar=True)
"""


t1 = TimerAnalyzer("/home/xabenshahar/data/temp/3000_platinum/", "CLEVER")

ys = t1.get_all_category_data("wallclock")

labels = ["1 omp X 8 mpi", "2 omp X 4 mpi", "4 omp X 2 mpi", "8 omp X 1 mpi"]
labels.reverse()

plot_data([labels], [ys], "mpi X omp", "runtime (seconds)", labels, "/home/xabenshahar/data/temp/3000_platinum/wallclock", "cleverleaf hybrid", bar=True)
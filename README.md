# CleverLeaf:
### description:
CleverLeaf project uses CloverLeaf's hydro kernels, and adds the AMR ability using SAMRAI library. use my git repositoty 'cleverleaf_copy' for the initial source code.
### prerequisites:
1. Add the following to the input file:
```
TimerManager {
    timer_list = "xfer::RefineSchedule::*","LagrangianEulerianLevelIntegrator::kernel::*"
    print_threshold = 0
}
```
this will alert which timers to use. In the above, we will get timers for the kernels, for the communication, and for the total runtime. <br /><br />
2. Add the following to the input file:
```
CartesianGeometry {
    threads      = 16
    x_lo         = 0.e0, 0.e0
    x_up         = 10.e0, 10.e0
    domain_boxes = [ (0,0), (1999,3999) ]
}
```
-> add the 'threads' field to the CartesianGeometry database, in order to control the thread number in runtime (a really BAD bandage).<br /><br />
3. Add the following to main.C:
```
int threads = input_db->getDatabase("CartesianGeometry")->getInteger("threads");
#if defined(_OPENMP)
    omp_set_dynamic(0);     // Explicitly disable dynamic teams
    omp_set_num_threads(threads);
#endif
```
<br />
4. In the CMakeLists.txt file, make sure to:<br />
a. add -fopenmp flag to CMAKE_CXX_FLAGS and to CMAKE_Fortran_FLAGS.<br />
b. add -D_OPENMP to 'add_definitions'
<br /><br />


5. Add the following to main.C:
```
std::string timers_out_dir_name = "TimersStatistics";
tbox::Utilities::recursiveMkdir(timers_out_dir_name);
std::string rank_str = std::to_string(rank);
std::string file_path = timers_out_dir_name + "/statistics_" + rank_str;
std::ofstream stat_file;
stat_file.open(file_path, std::ios::out);
tbox::TimerManager::getManager()->print(stat_file);
```
<br />
6. Disable writing visit dumps.
<br />
7. Override dt_new with 0.0004. <br /><br />

# CloverLeaf:
### description:
CloverLeaf is the origin project, more light-weighted, and very easy to install.
### prerequisites:
1. when compiling using 'make', sure sure to select a OpenMP compiler option (make COMPILER=GNU, ...). Otherwise, omp will not work.
2. Add the following to read_input.f90 file:<br />
a. line 32 - INTEGER            :: state,stat,state_max,n,threads<br />
b. line 54 - threads=1<br />
c. line 166:
```
CASE('threads')
    threads=parse_getival(parse_getword(.TRUE.))
    CALL OMP_SET_NUM_THREADS(threads);
    IF(parallel%boss)WRITE(g_out,"(1x,a25,i12)")'threads',threads
```


# performance.sh:
This file responsible to execute the programs and create the data. You can play with the configuration section in order to create different runs.


# performance.py:
This file responsible to analyse the data created within the runs. You can use the different functions and create different graphs.
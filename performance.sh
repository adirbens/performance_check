#!/bin/bash

run_cleverleaf=true
run_cloverleaf=true

load_gnu=true
load_intel=false # ----- #


curr_dir=$(pwd)
cleverleaf_exe_dir='/home/xabenshahar/opt/gnu/CleverLeaf/build/'
cloverleaf_exe_dir='/home/xabenshahar/opt/gnu/CloverLeaf'

cleverleaf_out_dir='' # ----- #
cloverleaf_out_dir='' # ----- #

# (1 4 9 16 25 36 49 64 81) # -> for cubic mode
task_count=(2 4 8)  #(1 2 4 8 12 16 20 24 28 32 40 48 56 64 72 80 88 96) 32 48 64 80 96
thread_count=(1 2 4)

enable_mpi=true
enable_omp=true

problem='triple_point' # ----- #
dt=0.0004 # ----- #

weak_scale=false # strong scaling if false

is_cubic=false

x_cells=30
y_cells=30

extra_mpi_options=' '
mpi_bindings=' '
mpi_use_all_nodes=false
mpi_report_bindings=true
mpi_map_by_numa=fasle
mpi_use_bindings=false



# -------------------- ^ CONFIGURATIONS ^ -------------------- #
# ------------------------------------------------------------ #
# ------------------------- v CODE v ------------------------- #


module load base-env
if [ $load_gnu = true ]
then

    module load compiler/gnu/11.2.0
    module load mpi/openmpi/4.1.5_gnu
    module load  

    export CC=mpicc
    export CXX=mpicxx
    
fi


row_factor=1; col_factor=1

if [ $enable_mpi = false ]; then task_count=(1); fi
if [ $enable_omp = false ]; then thread_count=(1); fi

if [ $mpi_use_all_nodes = true ]; then extra_mpi_options+=' --host rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 '; fi
if [ $mpi_report_bindings = true ]; then extra_mpi_options+=' --report-bindings '; fi
if [ $mpi_map_by_numa = true ]; then extra_mpi_options+=' --map-by numa '; fi

for mpi_rank in "${task_count[@]}"
    do

        if [[ $mpi_rank -gt 1 && $weak_scale = true ]]; then row_factor=2; fi
        if [[ $mpi_rank -gt 8 && $weak_scale = true ]]; then row_factor=4; fi
        if [[ $mpi_rank -gt 28 && $weak_scale = true ]]; then row_factor=8; fi
        if [[ $is_cubic = true && $weak_scale = true ]]; then row_factor=$(echo "sqrt($mpi_rank)" | bc); fi
        if [[ $weak_scale = true ]]; then col_factor=$(($mpi_rank / $row_factor)); fi

        for threads in "${thread_count[@]}"
            do
                
                if [ $mpi_use_bindings = true ]; then mpi_bindings=' --map-by ppr:'$((16 / $threads))':numa:pe='$(($threads)); fi
                #if [[ $threads*$mpi_rank -ne 96 ]]; then continue; fi

                if [ $run_cleverleaf = true ]
                then

                    cd $cleverleaf_exe_dir

                    cp ../test/template_triple_point.in ../test/triple_point.in

                    echo $'CartesianGeometry {' >> ../test/triple_point.in
                    echo $'    threads      = '$(($threads)) >> ../test/triple_point.in
                    echo $'    x_lo         = 0.e0, 0.e0' >> ../test/triple_point.in
                    echo $'    x_up         = 10.e0, 10.e0' >> ../test/triple_point.in
                    echo $'    domain_boxes = [ (0,0), ('$(($x_cells * $row_factor - 1))$','$(($y_cells * $col_factor - 1))$') ]' >> ../test/triple_point.in
                    echo $'\n}' >> ../test/triple_point.in
                    
                    mpirun -n $mpi_rank $extra_mpi_options $mpi_bindings ./src/cleverleaf ../test/triple_point.in
                    
                    cd TimersStatistics
                    mkdir $threads''_threads_mpi_rank_$mpi_rank
                    cp statistics_* $threads''_threads_mpi_rank_$mpi_rank
                    rm statistics_*
                    cd ..

                fi
                
                if [ $run_cloverleaf = true ]
                then

                    cd $cloverleaf_exe_dir
                    
                    cp template_clover.in clover.in

                    echo $' x_cells='$(($x_cells * $row_factor)) >> clover.in
                    echo $'\n y_cells='$(($y_cells * $col_factor)) >> clover.in
                    echo $'\n dtmax=0.0004' >> clover.in
                    echo $'\n dtmin=0.0004' >> clover.in
                    echo $'\n end_step=100' >> clover.in
                    echo $'\n threads='$(($threads)) >> clover.in
                    echo $'\n*endclover' >> clover.in

                    mpirun -n $mpi_rank $extra_mpi_options $mpi_bindings ./clover_leaf

                    cp /home/xabenshahar/opt/gnu/CloverLeaf/clover.out /home/xabenshahar/opt/gnu/CloverLeaf/output/$threads''_threads_mpi_rank_$mpi_rank

                fi
            done
        cd $curr_dir
    done
